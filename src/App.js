import React from 'react'
import './App.scss'
import ViewHeadlineIcon from '@material-ui/icons/ViewHeadline'
import LoginSection from './components/login-section/login-section'
import Connection from './services/connector'
import Sidebar from './components/sidebar/sidebar'
import ContentCreate from './components/content-create/content-create'
import ContentEvent from './components/content-event/content-event'
import EventList from './components/event-list/event-list'
import EventDetail from './components/event-detail/event-detail'
import AdminCreate from './components/admin-create/admin-create'

const clientUrl = process.env.REACT_APP_CLIENT_DOMAIN

let connection = new Connection()
window.nfc = connection

class App extends React.Component {
  constructor(props) {
    super(props)
    let localToken = localStorage.getItem('token')
    this.state = {
      IsSidebarActived: true,
      activeContent: 'list', // list, event
      session: localToken ? localToken : null,
      // session: '481794935a91a75b3c1b675e60585b3fd4c2fe6816d5b11977caf73384c85e01f0aafc93da34f0a2b2f45d79f7f5f7cb',
      userInfo: null,
      eventStat: {
        active: 0,
        available: 0,
        archive: 0
      },
      ignoreUpdate: false,
      topBarItemHeader: null,
      selectedEvent: null,
      firstLoading: true
    }
    window.clientUrl = clientUrl
  }

  setConnectionToken(token) {
    connection = new Connection(token)
    window.nfc = connection
  }

  componentWillMount() {
    document.addEventListener('visibilitychange', this.handleVisibilityChange, false)
    if (this.state.session) {
      this.setConnectionToken(this.state.session)
      this.checkSession()
      this.getEventStat()
      window.getEventStat = this.getEventStat
      window.onChangeActiveContent = this.onChangeActiveContent
      window.onToggleSidebar = this.onToggleSidebar
      window.onTriggerSlider = this.onTriggerSlider
      window.setTopBarHeader = this.setTopBarHeader
      window.onSetEvent = this.onSetEvent
      window.getDateTimeString = this.getDateTimeString
    } else {
      this.setState({
        firstLoading: false
      })
    }
  }

  getDateTimeString = (timestamp) => {
    let date = new Date(timestamp)
    let year = date.getFullYear()
    let month = date.getMonth() + 1
    if (month < 10) {
      month = '0' + month
    }
    let day = date.getDate()
    if (day < 10) {
      day = '0' + day
    }
    let hours = date.getHours()
    if (hours < 10) {
      hours = '0' + hours
    }
    let minutes = date.getMinutes()
    if (minutes < 10) {
      minutes = '0' + minutes
    }
    let seconds = date.getSeconds()
    if (seconds < 10) {
      seconds = '0' + seconds
    }
    return year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds
  }

  setTopBarHeader = (elements) => {
    this.setState({
      topBarItemHeader: elements
    })
  }

  getEventStat = async () => {
    return
    let result = await window.nfc.get('eventStat')
    if (result) {
      this.setState({
        eventStat: result
      })
    }
  }

  checkSession = async () => {
    let result = await window.nfc.get('session')
    if (result) {
      if (result.error) {
        localStorage.clear()
        window.location.reload()
        return
      }
      window.userInfo = result
      this.setState({
        userInfo: result,
        firstLoading: false
      })
    }
  }

  closeSlider() {
    this.refs['app-content'].style.width = '100%'
    this.refs['app-content'].style.left = '0px'
    this.refs['app-sidebar'].style.left = '-256px'
  }

  openSlider() {
    this.refs['app-content'].style.width = 'calc(100% - 256px)'
    this.refs['app-content'].style.left = '256px'
    this.refs['app-sidebar'].style.left = '0px'
  }

  onToggleSidebar = () => {
    if (!this.state.IsSidebarActived) {
      this.openSlider()
    } else {
      this.closeSlider()
    }
    this.setState({
      IsSidebarActived: !this.state.IsSidebarActived
    })
  }

  onTriggerSlider = (open) => {
    if (open !== undefined) {
      if (open) {
        this.openSlider()
      } else {
        this.closeSlider()
      }
      this.setState({
        IsSidebarActived: open
      })
    }
  }

  onSetEvent = (event) => {
    this.setState({
      selectedEvent: event
    })
  }

  onChangeActiveContent = (activeContent, slider) => {
    this.setState({
      activeContent: activeContent
    })
  }

  getActiveContent = () => {
    switch (this.state.activeContent) {
      case 'admin':
        return <AdminCreate />
      case 'create':
        return <ContentCreate onChangeActiveContent={this.onChangeActiveContent} />
      case 'list':
        return <EventList />
      case 'event':
        console.log('event: ', this.state.selectedEvent)
        return <EventDetail event={this.state.selectedEvent} />
      default:
        return <ContentEvent ignoreUpdate={this.state.ignoreUpdate} status={this.state.activeContent} onChangeActiveContent={this.onChangeActiveContent} />
    }
  }

  setUserInfo = (user) => {
    localStorage.setItem('token', user.token)
    window.location.reload()
  }

  render() {
    if (this.state.firstLoading) {
      return ''
    }
    if (!this.state.userInfo) {
      return <LoginSection setUserInfo={(e) => { this.setUserInfo(e) }} />
    }
    return <div className="app">
      <div ref={"app-sidebar"} className={"app-sidebar " + (this.state.IsSidebarActived ? 'active' : '')}>
        <Sidebar eventStat={this.state.eventStat} activeContent={this.state.activeContent} onChangeActiveContent={this.onChangeActiveContent} />
      </div>
      <div ref={"app-content"} className="app-content">
        <div className="top-bar">
          <div className="top-bar-list">
            <div className="top-bar-item">
              <ViewHeadlineIcon className="top-bar-icon" onClick={this.onToggleSidebar} className="btn" />
            </div>
            <div className="top-bar-item">
              {this.state.topBarItemHeader}
            </div>
          </div>
        </div>
        <div className="content-bar">
          {this.getActiveContent()}
        </div>
      </div>
    </div>
  }
}

export default App
