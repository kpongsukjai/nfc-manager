import React from 'react'
import './event-list.scss'
import Switch from '@material-ui/core/Switch'

class EventList extends React.Component {
    constructor(props) {
        super(props)
        let localStatus = localStorage.getItem('status')
        if (localStatus) {
            localStatus = JSON.parse(localStatus)
        }
        this.state = {
            events: [],
            status: localStatus || {
                Active: true,
                Idle: true,
                Closed: false
            }
        }
    }
    componentWillMount() {
        window.setTopBarHeader(this.getHeader())
        this.getEvents()
    }
    getHeader = () => {
        return <div className="event-list-header">
            <div className="event-list-header-item">
                Event List
            </div>
            <div className="event-list-header-item">
            </div>
        </div>
    }
    getEvents = async () => {
        let result = await window.nfc.post('getEvents', { status: this.state.status })
        if (!result.error) {
            this.setState({
                events: result
            })
        }
    }

    setEvent = (event) => {
        window.onChangeActiveContent('event')
        window.onSetEvent(event)
    }

    getEventRows = () => {
        let { events } = this.state
        return events.map((event) => {
            return <tr key={event.created_time} onClick={() => {
                this.setEvent(event)
            }}>
                <td>
                    {event.topic}
                </td>
                <td>
                    {event.description}
                </td>
                <td>
                    {event.zoneLength}
                </td>
                <td>
                    {event.status}
                </td>
            </tr>
        })
    }
    getSwitchElements = () => {
        const status = ['Active', 'Idle', 'Closed']
        return status.map((s) => {
            return [
                <Switch
                    key={"switch_" + s}
                    onChange={(e) => {
                        let status = this.state.status
                        status[s] = e.target.checked
                        localStorage.setItem('status', JSON.stringify(status))
                        this.setState({ status })
                        this.getEvents()
                    }}
                    defaultChecked={this.state.status[s]}
                    inputProps={{ 'aria-label': 'secondary checkbox' }}
                />,
                <span>{s}</span>
            ]
        })
    }
    render() {
        return <div className="event-list-section">
            <div className="event-list-status">
                {this.getSwitchElements()}
            </div>
            <div className="header-container">
                <table className="table-event-list">
                    <thead>
                        <tr>
                            <td>
                                Event
                        </td>
                            <td>
                                Description
                        </td>
                            <td>
                                Zones
                        </td>
                            <td>
                                Status
                        </td>
                        </tr>
                    </thead>
                </table>
            </div>
            <div className="body-container">
                <table className="table-event-list">
                    <tbody>
                        {this.getEventRows()}
                    </tbody>
                </table>
            </div>
        </div>
    }
}

export default EventList