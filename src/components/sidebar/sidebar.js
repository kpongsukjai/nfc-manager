import React from 'react'
import './sidebar.scss'
import ViewModuleIcon from '@material-ui/icons/ViewModule'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import Collapse from '@material-ui/core/Collapse'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import EventAvailableIcon from '@material-ui/icons/EventAvailable'
import DashboardIcon from '@material-ui/icons/Dashboard'
import EventIcon from '@material-ui/icons/Event'
import MeetingRoomIcon from '@material-ui/icons/MeetingRoom'
import PeopleIcon from '@material-ui/icons/People'

function ListItemLink(props) {
    return <ListItem button component="a" {...props} />;
}
class Sidebar extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            IsEventExpanded: true,
            activeContent: this.props.activeContent,
            eventStat: this.props.eventStat
        }
    }
    componentWillMount() {
        window.isActiveItem = this.isActiveItem
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.activeContent !== this.state.activeContent) {
            this.setState({
                activeContent: nextProps.activeContent
            })
        }
        if (nextProps.eventStat !== this.state.eventStat) {
            this.setState({
                eventStat: nextProps.eventStat
            })
        }
    }
    handleClickEvents = () => {
        this.setState({
            IsEventExpanded: !this.state.IsEventExpanded
        })
    }
    isActiveItem = (item) => {
        return this.state.activeContent === item ? 'active' : ''
    }
    logout = () => {
        localStorage.clear()
        window.location.reload()
    }
    render() {
        console.log(this.state)
        return <div className="sidebar-container">
            <div className="sidebar-section header">
                NFC <span className="role">
                    ({window.userInfo.role.toUpperCase()} Account)
                </span>
            </div>
            <div className="sidebar-section">
                <List component="nav" aria-label="main mailbox folders">
                    {
                        false ?
                            <ListItem button onClick={this.handleClickEvents}>
                                <ListItemIcon>
                                    <DashboardIcon />
                                </ListItemIcon>
                                <ListItemText primary="Dashboard" />
                            </ListItem>
                            :
                            null
                    }
                    <ListItem button onClick={this.handleClickEvents}>
                        <ListItemIcon>
                            <EventIcon />
                        </ListItemIcon>
                        <ListItemText primary="Events" />
                    </ListItem>
                    <Collapse className="menu-collapse" in={this.state.IsEventExpanded} timeout="auto" unmountOnExit>
                        <List component="div" disablePadding>
                            {
                                window.userInfo.role === 'root' ?
                                    <ListItem className={this.isActiveItem('create')} button onClick={() => { this.props.onChangeActiveContent('create') }}>
                                        <ListItemIcon>
                                            <AddCircleIcon />
                                        </ListItemIcon>
                                        <ListItemText primary={"Create"} />
                                    </ListItem>
                                    :
                                    null
                            }
                            <ListItem className={this.isActiveItem('list')} button onClick={() => { this.props.onChangeActiveContent('list') }}>
                                <ListItemIcon>
                                    <EventAvailableIcon />
                                </ListItemIcon>
                                <ListItemText primary={"Event List"} />
                            </ListItem>
                        </List>
                    </Collapse>
                    {
                        window.userInfo.role === 'root' ?
                            <List component="div" disablePadding>
                                <ListItem className={this.isActiveItem('admin')} button onClick={() => { this.props.onChangeActiveContent('admin') }}>
                                    <ListItemIcon>
                                        <PeopleIcon />
                                    </ListItemIcon>
                                    <ListItemText primary="Administrator" />
                                </ListItem>
                            </List>
                            :
                            null
                    }
                </List>
                <List>
                    <ListItem onClick={this.logout} className="logout" button>
                        <ListItemIcon>
                            <MeetingRoomIcon />
                        </ListItemIcon>
                        <ListItemText primary="Logout" />
                    </ListItem>
                </List>
            </div>
        </div>
    }
}

export default Sidebar
