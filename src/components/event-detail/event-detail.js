import React from 'react'
import EventZones from '../event-zones/event-zones'
import EventZoneLogs from '../event-zone-logs/event-zone-logs'
import './event-detail.scss'
import { TextField } from '@material-ui/core'
import Button from '@material-ui/core/Button'
import SaveIcon from '@material-ui/icons/Save'
import Switch from '@material-ui/core/Switch'
import EventAttendees from '../event-attendees/event-attendees'
import EditIcon from '@material-ui/icons/Edit'
import EventAttendee from '../event-attendee/event-attendee'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline'
import DeleteIcon from '@material-ui/icons/Delete'

class EventDetail extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            event: this.props.event,
            fullscreen: this.props.fullscreen,
            selectedTab: 'detail', // zones, zone
            selectedZone: null,
            selectedAttendee: null,
            attendeesCount: null,
            zonesCount: null,
        }
    }
    componentWillMount() {
        this.setTopBarHeader()
        window.onSetSelectedZone = this.onSetSelectedZone
        window.onSetSelectedAttendee = this.onSetSelectedAttendee
        this.getSummaryInfo()
    }

    setTopBarHeader = (event) => {
        window.setTopBarHeader(this.getHeader(event))
    }
    getHeader = (event) => {
        const e = event || this.state.event
        if (!e) return ''
        return <div>
            {e.topic}
        </div>
    }
    componentWillReceiveProps(nextProps) {
        const { event } = nextProps
        if (event !== this.state.event) {
            this.setState({ event })
            this.setTopBarHeader(event)

        }
    }
    startEvent = async () => {
        let result = await window.nfc.post('startEvent', {
            created_time: this.state.event.created_time
        })
        if (!result.error) {
            this.setState({
                event: result
            })
        }
    }
    closeEvent = async () => {
        let result = await window.nfc.post('closeEvent', {
            created_time: this.state.event.created_time
        })
        if (!result.error) {
            this.setState({
                event: result
            })
        }
    }
    openDashboard = () => {
        window.open(window.clientUrl + '?event=' + this.state.event.created_time + '&topic=' + this.state.event.topic + '&menu=dashboard', '_blank')
    }
    cancelEvent = async () => {
        let result = await window.nfc.post('cancelEvent', {
            created_time: this.state.event.created_time
        })
        if (!result.error) {
            this.setState({
                event: result
            })
        }
    }
    deleteEvent = async () => {
        let result = await window.nfc.post('deleteEvent', {
            created_time: this.state.event.created_time
        })
        if (result && result.status === 'success') {
            window.onSetEvent(undefined)
            window.onChangeActiveContent('list')
        }
    }
    register = () => {
        window.open(window.clientUrl + '?event=' + this.state.event.created_time + '&topic=' + this.state.event.topic + '&token=' + localStorage.getItem('token') + '&state=register', '_blank')
    }
    setEvent = () => {
        window.open(window.clientUrl + '?event=' + this.state.event.created_time + '&topic=' + this.state.event.topic, '_blank')
    }
    setEventCheckout = () => {
        window.open(window.clientUrl + '?event=' + this.state.event.created_time + '&topic=' + this.state.event.topic + '&status=checkout', '_blank')
    }
    onChangeEventSize = (val) => {
        this.setState({
            sizeChanged: val
        })
    }
    saveEventChanged = async () => {
        this.setState({
            disabledSaving: true
        })
        let result = await window.nfc.post('saveEventChanged', {
            event: {
                created_time: this.state.event.created_time,
                status: this.state.event.status
            },
            sizeChanged: this.state.sizeChanged
        })
        this.setState({
            disabledSaving: false
        })
        if (!result.error) {
            let event = this.state.event
            event.size = this.state.sizeChanged
            this.setState({
                event
            })
            alert('Save changed.')
        }
    }
    isSelectedTab = (tab) => {
        return this.state.selectedTab === tab ? 'selected-tab' : ''
    }
    getSummaryInfo = async () => {
        let result = await window.nfc.post('getSummaryInfo', {
            created_time: this.state.event.created_time
        })
        if (!result.error) {
            this.setState({
                attendeesCount: result.attendeesCount,
                zonesCount: result.zonesCount
            })
        }
    }
    onSelectTab = (tab) => {
        this.getSummaryInfo()
        this.setState({
            selectedTab: tab
        })
    }
    onSetSelectedZone = (zone) => {
        this.setState({
            selectedZone: zone,
            selectedTab: 'zone'
        })
    }
    onSetSelectedAttendee = (attendee) => {
        this.setState({
            selectedAttendee: attendee,
            selectedTab: 'attendee'
        })
    }
    editDeleteDay = async (event) => {
        let day = window.prompt('Edit Delete Day', event.deleteDay)
        if (day === null || day == 0) return
        day = parseInt(day)
        if (Number.isInteger(day)) {
            let result = await window.nfc.post('editDeleteDay', {
                created_time: event.created_time,
                day: day
            })
            if (!result.error) {
                this.setState({
                    event: result
                })
            }
        }
    }
    editEventUrl = async (event) => {
        let url = window.prompt('Edit Event URL', event.url)
        if (url === null) return
        if (!url) {
            url = ''
        }
        let result = await window.nfc.post('editEventUrl', {
            created_time: event.created_time,
            url: url
        })
        if (!result.error) {
            this.setState({
                event: result
            })
        }
    }
    addConsent = async (consentId, prevText) => {
        let text = window.prompt("Consent " + consentId, prevText)
        if (text) {
            let result = await window.nfc.post('addConsent', {
                created_time: this.state.event.created_time,
                consentId: consentId,
                text: text
            })
            if (!result.error) {
                this.setState({
                    event: result
                })
            }
        }
    }
    getConsents = () => {
        let { consents } = this.state.event
        return consents.map((consent) => {
            return <div className="age-range-label">
                &emsp;- {consent.text} <DeleteIcon onClick={() => { this.deleteConsent(consent.id) }} className="btn" />
            </div>
        })
    }
    getAgeRanges = () => {
        let { ageRanges } = this.state.event
        if (!ageRanges) {
            ageRanges = []
        }
        return ageRanges.map((ageRange) => {
            return <div className="age-range-label">
                &emsp;- {ageRange.text} <DeleteIcon onClick={() => { this.deleteAgeRange(ageRange.id) }} className="btn" />
            </div>
        })
    }
    deleteAgeRange = async (id) => {
        let result = await window.nfc.post('deleteAgeRange', {
            created_time: this.state.event.created_time,
            id: id
        })
        if (!result.error) {
            this.setState({
                event: result
            })
        }
    }
    deleteConsent = async (id) => {
        let result = await window.nfc.post('deleteConsent', {
            created_time: this.state.event.created_time,
            id: id
        })
        if (!result.error) {
            this.setState({
                event: result
            })
        }
    }
    addConsent = async (event) => {
        let value = window.prompt('Add consent')
        if (value === null) return
        if (value) {
            let result = await window.nfc.post('addConsent', {
                created_time: event.created_time,
                value: value
            })
            if (!result.error) {
                this.setState({
                    event: result
                })
            }
        }
    }
    addAgeRange = async (event) => {
        let value = window.prompt('Age Range (e.g. 18-25, มากกว่า 25)')
        if (value === null) return
        if (value) {
            let result = await window.nfc.post('addAgeRanges', {
                created_time: event.created_time,
                value: value
            })
            if (!result.error) {
                this.setState({
                    event: result
                })
            }
        }
    }
    getDetailTabContent = () => {
        const { event } = this.state
        if (!event) {
            return ''
        }
        return <div className="detail-content">
            <div>
                <table className="table-detail">
                    <tr>
                        <td colSpan={2}>{event.topic} ({event.status})</td>
                    </tr>
                    <tr>
                        <td>
                            Description
                    </td>
                        <td>
                            {event.description}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Created time
                    </td>
                        <td>
                            {new Date(event.created_time).toLocaleString()}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Started time
                    </td>
                        <td>
                            {event.started_time ? new Date(event.started_time).toLocaleString() : 'N/A'}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Closed time
                        </td>
                        <td>
                            {event.closed_time ? new Date(event.closed_time).toLocaleString() : 'N/A'}
                        </td>
                    </tr>
                    {
                        this.state.event.status === 'Closed' ?
                            <tr>
                                <td>
                                    Event URL
                                </td>
                                <td>
                                    {event.url ? <a href={event.url} target="_blank">{event.url}</a> : 'N/A'} <EditIcon className="btn" onClick={() => { this.editEventUrl(event) }} />
                                </td>
                            </tr>
                            :
                            null
                    }
                    <tr>
                        <td>
                            Delete Days
                        </td>
                        <td>
                            {event.deleteDay} {window.userInfo.role === 'root' ? <EditIcon className="btn" onClick={() => { this.editDeleteDay(event) }} /> : null}
                        </td>
                    </tr>
                    <tr>
                        <td className="age-ranges-label">
                            <span>Consents </span>
                            <span><AddCircleOutlineIcon onClick={() => { this.addConsent(event) }} className="btn" /></span>
                        </td>
                    </tr>
                    {
                        this.state.event.consents && this.state.event.consents.length > 0
                            ?
                            <tr>
                                <td colSpan={2}>
                                    {
                                        this.getConsents()
                                    }
                                </td>
                            </tr>
                            :
                            null
                    }
                    <tr>
                        <td className="age-ranges-label">
                            <span>Age Ranges </span>
                            <span><AddCircleOutlineIcon onClick={() => { this.addAgeRange(event) }} className="btn" /></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            {
                                this.getAgeRanges()
                            }
                        </td>
                    </tr>
                </table>
            </div>
            <div className="end-section">
                <Button disabled={this.state.event.status === 'Closed'} onClick={this.register} className="success">Rergister</Button>
                &nbsp;
                {
                    window.userInfo.role === 'root' ?
                        <span>
                            <Button disabled={this.state.event.status === 'Active' || this.state.event.status === 'Closed'} onClick={this.startEvent} color="primary" variant="contained">Start</Button>
                    &nbsp;
                    <Button disabled={this.state.event.status !== 'Active'} onClick={this.cancelEvent} color="primary" variant="contained">Cancel</Button>
                    &nbsp;
                    <Button onClick={this.closeEvent} disabled={this.state.event.status !== 'Active'} color="primary" variant="contained">Close </Button>
                    &nbsp;
                    <Button onClick={this.deleteEvent} className="danger">Delete </Button>
                        </span>
                        :
                        null
                }
            </div>
        </div>
    }
    getZonesTabContent = () => {
        return <EventZones event={this.state.event} />
    }
    getEventZoneLogs = () => {
        return <EventZoneLogs event={this.state.event} zone={this.state.selectedZone} />
    }
    getEventAttendees = () => {
        return <EventAttendees event={this.state.event} />
    }

    getEventAttendee = () => {
        return <EventAttendee event={this.state.event} attendee={this.state.selectedAttendee} />
    }

    getTabContent = () => {
        let tab = this.state.selectedTab
        switch (tab) {
            case 'detail':
                return this.getDetailTabContent()
            case 'zones':
                return this.getZonesTabContent()
            case 'zone':
                return this.getEventZoneLogs()
            case 'attendees':
                return this.getEventAttendees()
            case 'attendee':
                return this.getEventAttendee()
        }
    }
    activeEvent = () => {

    }
    render() {
        return <div className="event-detail-section">
            <div className="event-detail-tabs">
                <div onClick={() => { this.onSelectTab('detail') }} className={"tab-item " + this.isSelectedTab('detail')}>
                    Detail
                </div>
                <div onClick={() => { this.onSelectTab('attendees') }} className={"tab-item " + this.isSelectedTab('attendees')}>
                    Attendees {this.state.attendeesCount !== null ? '(' + this.state.attendeesCount + ')' : null}
                </div>
                <div onClick={() => { this.onSelectTab('zones') }} className={"tab-item " + this.isSelectedTab('zones')}>
                    Zones {this.state.zonesCount !== null ? '(' + this.state.zonesCount + ')' : null}
                </div>
                {
                    this.state.selectedZone ?
                        <div onClick={() => { this.onSelectTab('zone') }} className={"tab-item " + this.isSelectedTab('zone')}>
                            {this.state.selectedZone.name}
                        </div>
                        :
                        null
                }
                {
                    this.state.selectedAttendee ?
                        <div onClick={() => { this.onSelectTab('attendee') }} className={"tab-item " + this.isSelectedTab('attendee')}>
                            Attendee
                        </div>
                        :
                        null
                }
            </div>
            <div className="event-detail">
                {
                    this.getTabContent()
                }
            </div>
        </div>
    }
}

export default EventDetail