import React from 'react'
import './event-attendees.scss'
import Button from '@material-ui/core/Button'
import DeleteIcon from '@material-ui/icons/Delete'

class EventAttendees extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            event: this.props.event,
            attendees: []
        }
    }
    componentWillReceiveProps(nextProps) {
        console.log(nextProps.event)
        if (nextProps.event !== this.state.event) {
            this.setState({
                event: nextProps.event
            })
        }
    }
    componentWillMount() {
        this.getAttendees()
    }
    getAttendees = async () => {
        if (this.state.event) {
            let result = await window.nfc.post('getAttendees', {
                created_time: this.state.event.created_time
            })
            if (!result.error) {
                this.setState({
                    attendees: result
                })
            }
        }
    }
    deleteAttendee = async (attendee) => {
        let result = await window.nfc.post('deleteAttendee', {
            created_time: this.state.event.created_time,
            tagId: attendee.tagId
        })
        if (!result.error) {
            this.setState({
                attendees: result
            })
        }
    }
    getAttendeeRows = () => {
        let attendees = this.state.attendees
        return attendees.map((attendee) => {
            return <tr>
                <td>{attendee.tagId}</td>
                <td>{attendee.name} {attendee.surname}</td>
                <td>{attendee.gender}</td>
                <td>{attendee.age}</td>
                <td>{attendee.mobileNo}</td>
                <td>{attendee.zones[0].name}</td>
                <td>
                    <Button onClick={() => { window.onSetSelectedAttendee(attendee) }}>View Logs</Button>
                    {
                        this.state.event.status !== 'Closed' ?
                            <Button onClick={() => { this.deleteAttendee(attendee) }}>
                                <DeleteIcon />
                            </Button>
                            :
                            null
                    }
                </td>
            </tr>
        })
    }
    render() {
        return <div className="attendees-section">
            {
                this.state.attendees.length === 0 ?
                    <div>
                        <center>
                            There is no attendee.
                        </center>
                    </div>
                    :
                    <table className="table-attendees">
                        <thead>
                            <tr>
                                <td>Tag ID</td>
                                <td>Name - Surname</td>
                                <td>Gender</td>
                                <td>Age</td>
                                <td>Mobile No.</td>
                                <td>Zone</td>
                                <td>

                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            {this.getAttendeeRows()}
                        </tbody>
                    </table>
            }
        </div>
    }
}

export default EventAttendees