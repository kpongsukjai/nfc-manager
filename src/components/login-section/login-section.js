import React from 'react'
import { TextField, Button } from '@material-ui/core'
import './login-section.scss'

class LoginSection extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            form: {
                username: '',
                password: ''
            }
        }
    }
    changeForm = (field, value) => {
        let { form } = this.state
        form[field] = value
    }
    login = async () => {
        let { form } = this.state
        let result = await window.nfc.post('login', form)
        if (!result.error) {
            this.props.setUserInfo(result)
        } else {
            let passwordInput = this.refs['password'].getElementsByTagName('input')[0]
            passwordInput.value = ''
            passwordInput.focus()
        }
    }
    keyDown = (e) => {
        if (e.key === 'Enter') {
            this.login()
        }
    }
    render() {
        return <div className="login-section">
            <div className="login-input-section">
                NFC Login
            </div>
            <div className="login-input-section">
                <TextField ref='username' onKeyPress={this.keyDown} onChange={(e) => { this.changeForm('username', e.target.value) }} type="text" label="USERNAME" variant="outlined" autoFocus={true} />
            </div>
            <div className="login-input-section">
                <TextField ref='password' onKeyPress={this.keyDown} onChange={(e) => { this.changeForm('password', e.target.value) }} type="password" label="PASSWORD" variant="outlined" />
            </div>
            <div className="login-input-section">
                <Button onClick={this.login} variant="contained" color="primary">
                    Login
                </Button>
            </div>

        </div>
    }
}

export default LoginSection