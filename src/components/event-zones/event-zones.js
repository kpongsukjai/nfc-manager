import React from 'react'
import './event-zones.scss'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'
import PageviewIcon from '@material-ui/icons/Pageview'
import FingerprintIcon from '@material-ui/icons/Fingerprint'

class EventZones extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            event: this.props.event,
            zones: []
        }
    }
    componentWillMount() {
        this.getZones()
    }
    componentWillUnmount() {
        if (this.zoneTimer) {
            clearTimeout(this.zoneTimer)
        }
    }
    getZones = async () => {
        let result = await window.nfc.post('getZones', {
            created_time: this.state.event.created_time
        })
        if (!result.error) {
            this.setState({
                zones: result
            })
            if (this.state.event.status === 'Active') {
                this.zoneTimer = setTimeout(() => {
                    this.getZones()
                }, 1000)
            }
        }
    }
    createZone = async () => {
        let zoneName = this.refs['zoneName'].getElementsByTagName('INPUT')[0]
        let capacity = this.refs['capacity'].getElementsByTagName('INPUT')[0]
        if (zoneName.value === '') {
            alert('Invalid Zone Name')
            zoneName.focus()
            return
        }
        if (capacity.value === '' || isNaN(capacity.value)) {
            alert('Invalid Capacity')
            capacity.focus()
            return
        }
        let result = await window.nfc.post('createZone', {
            created_time: this.state.event.created_time,
            zoneName: zoneName.value,
            capacity: capacity.value
        })
        if (!result.error) {
            this.setState({
                zones: result
            })
            zoneName.value = ''
            capacity.value = ''
            zoneName.focus()
        }
    }

    deleteZone = async (zone) => {
        let confirm = window.confirm("Are you sure to delete zone: " + zone.name)
        if (confirm) {
            let result = await window.nfc.post('deleteZone', {
                created_time: this.state.event.created_time,
                zone: {
                    created_time: zone.created_time
                }
            })
            if (!result.error) {
                this.setState({
                    zones: result
                })
            }
        } else {
        }
    }

    setScanner = (zone) => {
        const url = window.clientUrl + '?event=' + this.state.event.created_time + '&topic=' + this.state.event.topic + '&zone=' + zone.created_time + '&name=' + zone.name + '&state=scanner' + '&token=' + localStorage.getItem('token')
        window.open(url, '_blank')
    }

    editZone = async (zone) => {
        let capacity = prompt("Enter new capacity", zone.size)
        if (capacity === null) return
        capacity = parseInt(capacity)
        if (Number.isInteger(capacity)) {
            let result = await window.nfc.post('editZone', {
                created_time: this.state.event.created_time,
                zone: {
                    created_time: zone.created_time,
                    capacity: capacity
                }
            })
            if (!result.error) {
                this.setState({
                    zones: result
                })
            }
        } else {
            alert('Invalid capacity input')
            this.editZone(zone)
        }
    }

    openZoneDashboard = (zone) => {
        const url = window.clientUrl + '?event=' + this.state.event.created_time + '&topic=' + this.state.event.topic + '&zone=' + zone.created_time + '&name=' + zone.name + '&menu=dashboard' + '&token=' + localStorage.getItem('token')
        window.open(url, '_blank')
    }

    getZoneRows = () => {
        const zones = this.state.zones
        return zones.map((zone) => {
            return <tr key={"zone_" + zone.created_time}>
                <td>{zone.name}</td>
                <td>{zone.count ? zone.count : '0'}/{zone.size}</td>
                <td>
                    <Button variant="contained" onClick={() => { this.openZoneDashboard(zone) }}>Dashboard</Button>
                    <Button variant="contained" onClick={() => { window.onSetSelectedZone(zone) }}>View logs</Button>
                    {
                        this.state.event.status !== 'Closed' && this.state.event.status === 'Active' ?
                            <Button onClick={() => { this.setScanner(zone) }}><FingerprintIcon /></Button>
                            :
                            null
                    }
                    {
                        this.state.event.status !== 'Closed' && window.userInfo.role === 'root' ?
                            <Button onClick={() => { this.editZone(zone) }}><EditIcon /></Button>
                            :
                            null
                    }
                    {
                        this.state.event.status !== 'Closed' && window.userInfo.role === 'root' ?
                            <Button onClick={() => { this.deleteZone(zone) }}><DeleteIcon /></Button>
                            :
                            null
                    }
                </td>
            </tr>
        })
    }

    onKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.createZone()
        }
    }

    render() {
        return <div className="zone-section">
            {
                this.state.event.status !== 'Closed' && window.userInfo.role === 'root' ?
                    <div className="create-zone-section">
                        <div className="create-list">
                            <TextField onKeyPress={this.onKeyPress} ref="zoneName" type="text" label="Zone Name" variant="outlined" />
                        </div>
                        <div className="create-list">
                            <TextField onKeyPress={this.onKeyPress} ref="capacity" type="number" label="Capacity" variant="outlined" />
                        </div>
                        <div className="create-list create-btn">
                            <Button onClick={this.createZone} color="primary" variant="contained">
                                Create
                            </Button>
                        </div>
                    </div>
                    :
                    null
            }
            {
                this.state.zones.length === 0 ?
                    <div>
                        <center>
                            There is no zone.
                        </center>
                    </div>
                    :
                    <div className="zones-section">
                        <table className="table-zones">
                            <thead>
                                <td>Zone Name</td>
                                <td>Capcity</td>
                                <td></td>
                            </thead>
                            <tbody>
                                {this.getZoneRows()}
                            </tbody>
                        </table>
                    </div>
            }
        </div>
    }
}

export default EventZones