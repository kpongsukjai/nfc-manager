import React from 'react'
import './admin-create.scss'
import { TextField, Button } from '@material-ui/core/'
import VpnKeyIcon from '@material-ui/icons/VpnKey'
import DeleteIcon from '@material-ui/icons/Delete'

class AdminCreate extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            accounts: []
        }
    }
    componentWillMount() {
        window.setTopBarHeader('Administrator')
        this.getAccounts()
    }
    getAccounts = async () => {
        let result = await window.nfc.get('accounts')
        if (!result.error) {
            this.setState({
                accounts: result
            })
        }
    }
    createAccount = async () => {
        let usernameInput = this.refs['username'].getElementsByTagName('INPUT')[0]
        let passwordInput = this.refs['password'].getElementsByTagName('INPUT')[0]
        const form = {
            username: usernameInput.value,
            password: passwordInput.value
        }
        if (form.username.length <= 3) {
            alert('username length must more than 3')
            return
        }
        if (!form.password) {
            return
        }
        let result = await window.nfc.post('createAccount', form)
        if (!result.error) {
            this.setState({
                accounts: result
            })
        }
        usernameInput.value = ''
        passwordInput.value = ''
        usernameInput.focus()
    }
    deleteAccount = async (user) => {
        let confirm = window.confirm('username: ' + user.username + "\n" + 'Are you sure to delete this account ?')
        if (confirm) {
            let result = await window.nfc.post('deleteAccount', {
                user_id: user.user_id
            })
            if (!result.error) {
                this.setState({
                    accounts: result
                })
            }
        }
    }
    changeAccountPassword = async (user) => {
        let password = window.prompt('username: ' + user.username + "\n" + 'Enter new password:')
        if (password === null) return
        if (password) {
            let result = await window.nfc.post('changeAccountPassword', {
                user_id: user.user_id,
                password: password
            })
            if (!result.error) {
                alert('username: ' + user.username + "\n" + 'Password is changed.')
            }
        }
    }
    onKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.createAccount()
        }
    }
    getAccountRows = () => {
        let accounts = this.state.accounts
        return accounts.map((account) => {
            return <div className="account-item">
                <div className="account-username">
                    {account.username}
                </div>
                <div className="account-action">
                    <Button onClick={() => { this.changeAccountPassword(account) }}><VpnKeyIcon /></Button>
                    <Button onClick={() => { this.deleteAccount(account) }} ><DeleteIcon /></Button>
                </div>
            </div>
        })
    }
    render() {
        return <div className="admin-create-section">
            <div className="create-form">
                <div className="create-form-item">
                    <TextField onKeyPress={this.onKeyPress} ref='username' label="Username" variant="outlined" />
                </div>
                <div className="create-form-item">
                    <TextField onKeyPress={this.onKeyPress} ref='password' label="Username" variant="outlined" />
                </div>
                <div className="create-form-item">
                    <Button onClick={this.createAccount} color="primary" variant="contained">Create</Button>
                </div>
            </div>
            <div className="admin-data">
                {
                    this.getAccountRows()
                }
            </div>
        </div>
    }
}

export default AdminCreate