import React from 'react'
import './event-detail-actions.scss'

class EventDetailActions extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            event: this.props.event
        }
    }
    componentWillReceiveProps(nextProps) {
        if (this.state.event != nextProps.event) {
            this.setState({
                event: this.props.event
            })
        }
    }
    actionsBodyRows = () => {
        let actions = this.state.event.actions
        return actions.map((action) => {
            let status = 'check in'
            if (action.checkIn && action.checkOut) {
                status = 'check out'
            }
            return <tr>
                <td>{new Date(action.checkIn).toLocaleTimeString()}</td>
                <td>{action.checkOut ? new Date(action.checkOut).toLocaleTimeString() : ''}</td>
                <td>{action.name}</td>
                <td>{action.mobileNo}</td>
            </tr>
        })
    }
    render() {
        return <div className="event-detail-action-section">
            {
                this.state.event.count !== undefined ?
                    <div className="count-status">
                        Capacity {this.state.event.count}/{this.state.event.size}
                    </div>
                    :
                    null
            }
            <table className="action-table action-table-header">
                <thead>
                    <tr>
                        <td>In</td>
                        <td>Out</td>
                        <td>Name</td>
                        <td>Mobile No.</td>
                    </tr>
                </thead>
            </table>
            <div>
                <table className="action-table action-table-body">
                    <tbody>
                        {this.actionsBodyRows()}
                    </tbody>
                </table>
            </div>
        </div>
    }
}

export default EventDetailActions