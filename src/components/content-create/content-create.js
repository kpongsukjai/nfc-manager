import React from 'react'
import './content-create.scss'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

class ContentCreate extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            form: {
                topic: '',
                description: '',
                size: 0
            }
        }
    }
    componentWillMount() {
        window.setTopBarHeader(<div>Create Event</div>)
    }
    onChangeForm = (field, value) => {
        let form = this.state.form
        form[field] = value
        this.setState({
            form
        })
    }
    create = async () => {
        let { form } = this.state
        let result = await window.nfc.post('createEvent', form)
        if (!result.error) {
            this.props.onChangeActiveContent('list')
        }
    }
    render() {
        return <div className="content-create-container">
            <div className="body">
                <div className="input-field">
                    <TextField onChange={(e) => { this.onChangeForm('topic', e.target.value) }} type="text" label="Topic" variant="outlined" />
                </div>
                <div className="input-field">
                    <TextField onChange={(e) => { this.onChangeForm('description', e.target.value) }} type="text" label="Description" variant="outlined" />
                </div>
            </div>
            <div className="footer">
                <Button onClick={() => { this.create() }} variant="contained" color="primary">
                    Create
                </Button>
            </div>
        </div>
    }
}

export default ContentCreate