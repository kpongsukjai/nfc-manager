import React from 'react'
import './event-attendee.scss'
import EditIcon from '@material-ui/icons/Edit'

class EventAttendee extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            event: this.props.event,
            attendee: this.props.attendee,
            logs: []
        }
    }
    componentWillMount() {
        this.getLogsAttendee()
    }
    getLogsAttendee = async () => {
        let result = await window.nfc.post('getLogsAttendee', {
            created_time: this.state.event.created_time,
            tagId: this.state.attendee.tagId
        })
        if (!result.error) {
            this.setState({
                logs: result
            })
        }
    }
    getLogRows = () => {
        let { logs } = this.state
        if (!logs || logs.length === 0) {
            return <tr key={"no_record"}>
                <td colSpan={3}>
                    <center>
                        No records
                    </center>
                </td>
            </tr>
        }
        return logs.map((log) => {
            const dateTimeString = window.getDateTimeString(log.created_time)
            return <tr key={"atttendee_log_" + log.created_time}>
                <td>{dateTimeString}</td>
                <td>{log.zone.name}</td>
                <td>{log.action}</td>
            </tr>
        })
    }
    changeAttendeeMobile = async () => {
        let mobileNo = window.prompt('Change mobile no.', this.state.attendee.mobileNo)
        if (mobileNo === null) return
        if (mobileNo) {
            let result = await window.nfc.post('changeAttendeeMobileNo', {
                created_time: this.state.event.created_time,
                tagId: this.state.attendee.tagId,
                mobileNo: mobileNo
            })
            if (!result.error) {
                this.setState({
                    attendee: result
                })
            }
        }
    }
    tableInfo = () => {
        const { attendee } = this.state
        return <table className="table-attendee-info">
            <tr>
                <td>Tag ID</td>
                <td>{attendee.tagId}</td>
            </tr>
            <tr>
                <td>Name</td>
                <td>{attendee.name} {attendee.surname}</td>
            </tr>
            <tr>
                <td>Gender</td>
                <td>{attendee.gender}</td>
            </tr>
            <tr>
                <td>Age</td>
                <td>{attendee.age}</td>
            </tr>
            <tr>
                <td>Mobile No.</td>
                <td>{attendee.mobileNo} {this.state.event.status !== 'Closed' ? <EditIcon onClick={this.changeAttendeeMobile} /> : null}</td>
            </tr>
            <tr>
                <td>Zone</td>
                <td>{attendee.zones[0].name}</td>
            </tr>
            <tr className="consent-row">
                <td>Accepted Consents</td>
                <td>
                    {
                        attendee.consents && attendee.consents.map((consent) => {
                            return <div>
                                -&nbsp;{consent.text}
                            </div>
                        })
                    }
                </td>
            </tr>
        </table>
    }
    render() {
        return <div className="attendee-logs-section">
            <div className="attendee-info">
                <center>
                    {this.tableInfo()}
                </center>
            </div>
            {
                this.state.logs === undefined ?
                    <div>
                        <center>
                            Loading...
                        </center>
                    </div>
                    :
                    <table className="attendee-table-logs">
                        <thead>
                            <tr>
                                <td>
                                    Time
                                </td>
                                <td>
                                    Zone
                                </td>
                                <td>
                                    Actions
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            {this.getLogRows()}
                        </tbody>
                    </table>
            }
        </div>
    }
}

export default EventAttendee