import React from 'react'
import "./content-event.scss"
import EventDetail from '../event-detail/event-detail'
import Switch from '@material-ui/core/Switch'
import EventZones from '../event-zones/event-zones'
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

class ContentEvent extends React.Component {
    constructor(props) {
        super(props)
        let realtime = localStorage.getItem('realtime')
        if (realtime) {
            realtime = JSON.parse(realtime)
        }
        let fullscreen = localStorage.getItem('fullscreen')
        if (fullscreen) {
            fullscreen = JSON.parse(fullscreen)
        }
        console.log(realtime)
        this.state = {
            events: [],
            status: this.props.status,
            selectedEvent: undefined,
            realtime: realtime || undefined,
            fullscreen: fullscreen || undefined
        }
    }
    componentWillReceiveProps(nextProps) {
        if (this.state.status !== nextProps.status) {
            if (nextProps.status !== 'active') {
                this.setState({
                    realtime: false
                })
            } else {
                if (this.state.realtime) {
                    this.runRealtimeData()
                }
            }
            this.setState({
                status: nextProps.status
            })
            window.getEventStat()
            if (!nextProps.ignoreUpdate) {
                window.getEvents(nextProps.status)
            }
        }
    }
    componentWillMount() {
        const urlParams = new URLSearchParams(window.location.search)
        let activeEvent = urlParams.get('activeEvent')
        if (activeEvent) {
            window.onChangeActiveContent('active')
        } else {
            this.getEvents()
        }
        window.removeSelectedEvent = this.removeSelectedEvent
        window.getEvents = this.getEvents
        if (this.state.realtime && this.state.status === 'active') {
            this.runRealtimeData()
        }
    }
    getEvents = async (optionalStatus) => {
        let result = await window.nfc.post('events', {
            status: optionalStatus || this.state.status
        })
        let selectedEvent = undefined
        if (this.state.status === 'active') {
            const urlParams = new URLSearchParams(window.location.search)
            let params = urlParams.get('activeEvent')
            if (params) {
                window.history.replaceState({}, document.title, "/")
            }
            for (let i in result) {
                if (result[i].created_time == params) {
                    selectedEvent = result[i]
                }
            }
        }
        if (result) {
            this.setState({
                events: result,
                selectedEvent: selectedEvent
            })
        }
    }
    onSelectEvent = (event) => {
        this.setState({
            selectedEvent: event
        })
    }
    removeSelectedEvent = () => {
        this.setState({
            selectedEvent: undefined
        })
        window.getEventStat()
        this.getEvents()
    }
    eventItems = () => {
        let events = this.state.events
        return events.map((event) => {
            let size = event.size
            if (this.state.status === 'active' && event.count !== undefined) {
                size = event.count + '/' + event.size
            }
            return <div key={event.created_time} className="event-item" onClick={() => { this.onSelectEvent(event) }}>
                <div className="event-item-column">
                    {event.topic}
                </div>
                <div className="event-item-column">
                    {event.description}
                </div>
                <div className="event-item-column">
                    {event.status}
                </div>
                <div className="event-item-column">
                    {event.zones.length}
                </div>
            </div>
        })
    }
    runRealtimeData = (optionNextRealtime) => {
        if (!optionNextRealtime && !this.state.realtime) return
        const selectedEvent = this.state.selectedEvent && this.state.selectedEvent.created_time
        window.nfc.post('realtime', {
            selectedEvent: selectedEvent
        }).then((result) => {
            if (!this.state.realtime) return
            if (selectedEvent) {
                const event = this.state.selectedEvent
                event.actions = result.actions
                event.count = result.count
                event.size = result.size
                this.setState({
                    selectedEvent: event
                })
            } else {
                let events = this.state.events
                for (let i in events) {
                    for (let j in result) {
                        if (events[i].created_time == result[j].created_time) {
                            events[i].count = result[j].count
                        }
                    }
                }
                this.setState({ events })
            }
        })
        setTimeout(() => {
            this.runRealtimeData()
        }, 1000)
    }
    onSwitchRealTime = () => {
        let nextRealtime = !this.state.realtime
        if (nextRealtime) {
            this.runRealtimeData(nextRealtime)
        }
        localStorage.setItem('realtime', JSON.stringify(nextRealtime))
        this.setState({
            realtime: nextRealtime
        })
    }
    onSwitchFullscreen = () => {
        let nextFullscreen = !this.state.fullscreen
        localStorage.setItem('fullscreen', JSON.stringify(nextFullscreen))
        this.setState({
            fullscreen: nextFullscreen
        })
    }

    render() {
        return <div className="content-event-container">
            <div className="header">
                {
                    this.state.selectedEvent ?
                        [
                            <span onClick={this.removeSelectedEvent} className="title">Event List ({this.state.status})</span>,
                            <span className="sub-header"> / {this.state.selectedEvent.topic}</span>
                        ]
                        :
                        <span>Event List ({this.state.status})</span>
                }
                {
                    this.state.status === 'active' || this.state.status === 'archive' ?
                        <div className="realtime-toggle">
                            {
                                this.state.selectedEvent ?
                                    <span>
                                        <Switch checked={this.state.fullscreen} onChange={this.onSwitchFullscreen} color="primary" /> Fullscreen Records
                                    </span>
                                    :
                                    null
                            }
                            {
                                this.state.status === 'active' ?
                                    <span>
                                        <Switch checked={this.state.realtime} onChange={this.onSwitchRealTime} color="primary" /> Realtime
                                    </span>
                                    :
                                    null
                            }
                        </div>
                        :
                        null
                }
            </div>
            {
                this.state.selectedEvent ?
                    <div>
                        <EventDetail event={this.state.selectedEvent}/>
                    </div>
                    :
                    (
                        this.state.events.length > 0
                            ?
                            <div className='body'>
                                <div className="event-list">
                                    <div className="event-header">
                                        <div className="event-header-item">
                                            Event
                            </div>
                                        <div className="event-header-item">
                                            Description
                            </div>
                                        <div className="event-header-item">
                                            Status
                            </div>
                                        <div className="event-header-item">
                                            Zones
                            </div>
                                    </div>
                                    {
                                        this.eventItems()
                                    }
                                </div>
                            </div>
                            :
                            <div className="no-event">
                                There is no event
                            </div>
                    )
            }
        </div>
    }
}

export default ContentEvent