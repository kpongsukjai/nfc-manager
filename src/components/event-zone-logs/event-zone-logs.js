import React from 'react'
import './event-zone-logs.scss'
import { Switch, TextField } from '@material-ui/core/'

class EventZoneLogs extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            event: this.props.event,
            zone: this.props.zone,
            logs: undefined,
            type: 'current',
            filter: ''
        }
    }
    componentWillMount() {
        this.getZoneLogs()
    }
    getZoneLogs = async (forceType) => {
        let result = await window.nfc.post('getZoneLogs', {
            created_time: this.state.event.created_time,
            zone: this.state.zone.created_time,
            type: forceType || this.state.type
        })
        if (!result.error) {
            this.setState({
                logs: result
            })
        }
    }
    getLogRows = () => {
        let { logs } = this.state
        if (logs === undefined) {
            return <tr>
                <td className="center" colSpan={5}>
                    Loading...
                </td>
            </tr>
        } else if (logs.length === 0) {
            return <tr>
                <td className="center" colSpan={5}>
                    There is no lgo in this zone
                </td>
            </tr>
        }
        const filter = this.state.filter.toLowerCase()
        logs = logs.filter((log) => {
            return log.name.toLowerCase().includes(filter) || log.tagId.toLowerCase().includes(filter) || log.mobileNo.toLowerCase().includes(filter)
        })
        return logs.map((log) => {
            const dateTimeString = window.getDateTimeString(log.created_time)
            return <tr>
                <td>{log.tagId}</td>
                <td>{log.name}</td>
                <td>{log.mobileNo}</td>
                <td>{dateTimeString}</td>
                <td>{log.action}</td>
            </tr>
        })
    }
    onChangeType = () => {
        const newType = this.state.type === 'current' ? 'records' : 'current'
        this.setState({
            type: newType
        })
        this.getZoneLogs(newType)
    }
    render() {
        return <div className="zone-logs-section">
            <div className="zone-logs-filters">
                <div className="zone-logs-filter">
                    <TextField onChange={(e) => { this.setState({ filter: e.target.value }) }} label="Search" variant="outlined" />
                </div>
                <div className="zone-logs-filter">
                    Current <Switch onChange={this.onChangeType} checked={this.state.type === 'records'} /> All Actions
                </div>
            </div>
            <div className="logs-section">
                {
                    this.state.logs === undefined ?
                        <div>
                            <center>
                                Loading...
                        </center>
                        </div>
                        :
                        <table className="table-log">
                            <thead>
                                <tr>
                                    <td>
                                        UUID
                                </td>
                                    <td>
                                        Name
                                </td>
                                    <td>
                                        Mobile No.
                                </td>
                                    <td>
                                        Time
                                </td>
                                    <td>
                                        Action
                                </td>
                                </tr>
                            </thead>
                            <tbody>
                                {this.getLogRows()}
                            </tbody>
                        </table>
                }
            </div>
        </div>
    }
}

export default EventZoneLogs