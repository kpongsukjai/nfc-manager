const url = process.env.REACT_APP_SERVER
class Connector {
    
    constructor(token) {
        this.connectorConfig = {
            token: token
        }
    }

    getHeaders() {
        let headers = {
            headers: {
                Authorization: this.connectorConfig.token,
                "Content-Type": 'application/json'
            }
        }
        if (!headers.headers.Authorization) {
            delete headers.headers.Authorization
        }
        return headers
    }

    async get(service) {
        const headers = this.getHeaders()
        try {
            let response = await fetch(url + '/' + service, {
                method: 'GET',
                headers: headers.headers
            })
            const json = await response.json()
            return json
        } catch (e) {
            console.log(e)
            alert('Service is errror, Please check details in log' + "\n" + e.message)
        }
    }
    async post(service, body) {
        const headers = this.getHeaders()
        try {
            let response = await fetch(url + '/' + service, {
                method: 'POST',
                headers: headers.headers,
                body: JSON.stringify(body),
                mode: 'cors'
            })
            const json = await response.json()
            if (!json) {
                throw new Error()
            } else {
                if (json.error) {
                    alert(json.error)
                }
                return json
            }
        } catch (e) {
            console.log(e)
            alert('Service is errror, Please check details in log' + "\n" + e.message)
            return {
                error: 'service_error'
            }
        }
    }
}

export default Connector